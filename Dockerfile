FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /opt/app
COPY target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar", "-web -webAllowOthers -tcp -tcpAllowOthers"]