package d.kenin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import d.kenin.model.Investor;
import d.kenin.repository.InvestorRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class InvestorControllerTest {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private InvestorRepository mockRepository;

    private static final ObjectMapper om = new ObjectMapper();

    private String entityBaseRequestPath = "/v1/investors";

    @Test
    void create__BAD_REQUEST_on_validation() throws Exception {
        // given
        String input =
                "{\n" +
                        "    \"name\": \"\",\n" +
                        "    \"surname\": \"\",\n" +
                        "    \"balance\": \"-1\"\n" +
                        "}";

        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("Validation failed for object='investor'. Error count: 3")))
                .andExpect(jsonPath("$.errors").isMap())
                .andExpect(jsonPath("$.errors.name", is(ERROR_MSG_IS_MANDATORY)))
                .andExpect(jsonPath("$.errors.surname", is(ERROR_MSG_IS_MANDATORY)))
                .andExpect(jsonPath("$.errors.balance", is("must be greater than or equal to 0")));

        verify(mockRepository, times(0)).save(any(Investor.class));
    }

    @Test
    void create_Success() throws Exception {
        // given
        String input =
                "{\n" +
                        "    \"name\": \"Dmitrijs\",\n" +
                        "    \"surname\": \"Kenins\",\n" +
                        "    \"balance\": 150.50\n" +
                        "}";
        Investor outputData = getEntityFrom(input);
        outputData.setId(1L);
        Mockito.when(mockRepository.save(any(Investor.class))).thenReturn(outputData);

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));

        // then
        verify(mockRepository, times(1)).save(any(Investor.class));
    }

    @Test
    void getAll_Success() throws Exception {
        // given
        List<Investor> outputData = getEntityListFrom(
                "[{\n" +
                        "        \"id\": 1,\n" +
                        "        \"name\": \"Dmitrijs\",\n" +
                        "        \"surname\": \"Kenins\",\n" +
                        "        \"balance\": \"250.00\"\n" +
                        "    }\n" +
                        "]");
        Mockito.when(mockRepository.findAll()).thenReturn(outputData);

        // when
        mockMvc.perform(get(entityBaseRequestPath)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$"). isArray())
                .andExpect(jsonPath("$[0].name", is("Dmitrijs")))
                .andExpect(jsonPath("$[0].surname", is("Kenins")))
                .andExpect(jsonPath("$[0].balance", is("250.00")));

        // then
        verify(mockRepository, times(1)).findAll();
    }

    private List<Investor> getEntityListFrom(String jsonInput) throws JsonProcessingException {
        return om.readerForListOf(Investor.class).readValue(jsonInput);
    }

    private Investor getEntityFrom(String jsonInput) throws JsonProcessingException {
        return om.readerFor(Investor.class).readValue(jsonInput);
    }
}