package d.kenin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import d.kenin.model.Loan;
import d.kenin.repository.LoanRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class LoanControllerTest {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LoanRepository mockRepository;

    private static final ObjectMapper om = new ObjectMapper();

    private String entityBaseRequestPath = "/v1/loans";

    @BeforeAll
    public static void beforeAllTests() {
        om.registerModule(new JavaTimeModule());
    }

    @Test
    void create__BAD_REQUEST_on_validation() throws Exception {
        // given
        String input =
                "{\n" +
                        "    \"amount\": null,\n" +
                        "    \"dueDate\": \"\",\n" +
                        "    \"monthlyRate\": 0\n" +
                        "}";

        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("Validation failed for object='loan'. Error count: 3")))
                .andExpect(jsonPath("$.errors").isMap())
                .andExpect(jsonPath("$.errors.amount", is(ERROR_MSG_IS_MANDATORY)))
                .andExpect(jsonPath("$.errors.dueDate", is(ERROR_MSG_IS_MANDATORY)))
                .andExpect(jsonPath("$.errors.monthlyRate", is("must be greater than 0")));

        verify(mockRepository, times(0)).save(any(Loan.class));
    }

    @Test
    void create_Success() throws Exception {
        // given
        String input =
                "{\n" +
                        "    \"amount\": 300.00,\n" +
                        "    \"dueDate\": \"2021-07-01\",\n" +
                        "    \"monthlyRate\": 0.2\n" +
                        "}";
        Loan outputData = getEntityFrom(input);
        outputData.setId(1L);
        Mockito.when(mockRepository.save(any(Loan.class))).thenReturn(outputData);

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));

        // then
        verify(mockRepository, times(1)).save(any(Loan.class));
    }

    @Test
    void getAll_Success() throws Exception {
        // given
        List<Loan> outputData = getEntityListFrom(
                "[\n" +
                        "    {\n" +
                        "        \"id\": 1,\n" +
                        "        \"amount\": \"300.00\",\n" +
                        "        \"dueDate\": \"2021-07-01\",\n" +
                        "        \"monthlyRate\": \"0.20\"\n" +
                        "    }\n" +
                        "]");
        Mockito.when(mockRepository.findAll()).thenReturn(outputData);

        // when
        mockMvc.perform(get(entityBaseRequestPath)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$"). isArray())
                .andExpect(jsonPath("$[0].amount", is("300.00")))
                .andExpect(jsonPath("$[0].dueDate", is("2021-07-01")))
                .andExpect(jsonPath("$[0].monthlyRate", is("0.20")));

        // then
        verify(mockRepository, times(1)).findAll();
    }

    private List<Loan> getEntityListFrom(String jsonInput) throws JsonProcessingException {
        return om.readerForListOf(Loan.class).readValue(jsonInput);
    }

    private Loan getEntityFrom(String jsonInput) throws JsonProcessingException {
        return om.readerFor(Loan.class).readValue(jsonInput);
    }

}