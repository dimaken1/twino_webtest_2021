package d.kenin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import d.kenin.exception.ResourceNotFoundException;
import d.kenin.model.AppConfig;
import d.kenin.repository.AppConfigRepository;
import d.kenin.service.AppConfigService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class AppConfigControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AppConfigService appConfigService;

    @MockBean
    private AppConfigRepository mockRepository;

    private static final ObjectMapper om = new ObjectMapper();

    private String entityBaseRequestPath = "/v1/config";

    /**
     * Context:    /api
     * <p>POST:    /v1/customers
     * <p>Body:    {"name": "","surname": null,"country": "","email": "@mail.com","password": ""}
     * <p>Error response:
     * <p>{
     * <p> "timestamp": "2021-07-02T15:45:48.193+00:00",
     * <p> "status": 400,
     * <p> "error": "Bad Request",
     * <p> "message": "Validation failed for entities: AppConfigController. Error count: 3",
     * <p> "errors": {
     * <p>     "create.configs[0].key": "is mandatory",
     * <p>     "create.configs[1].key": "is mandatory",
     * <p>     "create.configs[2].key": "is mandatory"
     * <p> },
     * <p> "path": "/api/v1/config/"
     * <p>}
     * @throws Exception
     */
    @Test
    void create__BAD_REQUEST_on_validation() throws Exception {
        // given
        String input =
                "[{\n" +
                "    \"key\": \"\",\n" +
                "    \"value\": \"6\"\n" +
                "},\n" +
                "{\n" +
                "    \"key\": \"\",\n" +
                "    \"value\": null\n" +
                "},\n" +
                "{\n" +
                "    \"key\": \"\",\n" +
                "    \"value\": \"WEEK\"\n" +
                "}]";

        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("Validation failed for entities: AppConfigController. Error count: 3")))
                .andExpect(jsonPath("$.errors").isMap())
                .andExpect(content().string(matchesPattern("(.*)\\[0\\].key\\\":\\\"is mandatory(.*)")))
                .andExpect(content().string(matchesPattern("(.*)\\[1\\].key\\\":\\\"is mandatory(.*)")))
                .andExpect(content().string(matchesPattern("(.*)\\[2\\].key\\\":\\\"is mandatory(.*)")));

        verify(mockRepository, times(0)).saveAll(any(List.class));
    }

    @Test
    void getByKey__NOT_FOUND() throws Exception {
        // given
        Mockito.when(mockRepository.findByKey("wrong_prop")).thenReturn(Optional.empty());

        // when
        mockMvc.perform(get(entityBaseRequestPath + "?key=wrong_prop")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message", is("The property was not found, requested key: wrong_prop")));

        // then
        verify(mockRepository, times(1)).findByKey(eq("wrong_prop"));
        assertThatThrownBy(() -> appConfigService.getByKey("wrong_prop"))
                .isInstanceOf(ResourceNotFoundException.class);
    }

    @Test
    void getAll_Success() throws Exception {
        // given
        List<AppConfig> outputData = getEntityListFrom(
                "[\n" +
                        "    {\n" +
                        "        \"id\": 1,\n" +
                        "        \"key\": \"period.max.investment.number\",\n" +
                        "        \"value\": \"6\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"id\": 2,\n" +
                        "        \"key\": \"investment.period.length\",\n" +
                        "        \"value\": \"2\"\n" +
                        "    },\n" +
                        "    {\n" +
                        "        \"id\": 3,\n" +
                        "        \"key\": \"investment.period.unit\",\n" +
                        "        \"value\": \"WEEK\"\n" +
                        "    }\n" +
                        "]");
        Mockito.when(mockRepository.findAll()).thenReturn(outputData);

        // when
        mockMvc.perform(get(entityBaseRequestPath)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$"). isArray())
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].key", is("period.max.investment.number")))
                .andExpect(jsonPath("$[0].value", is("6")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].key", is("investment.period.length")))
                .andExpect(jsonPath("$[1].value", is("2")))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].key", is("investment.period.unit")))
                .andExpect(jsonPath("$[2].value", is("WEEK")));

        // then
        verify(mockRepository, times(1)).findAll();
    }

    @Test
    void getByKey__Success() throws Exception {
        // given
        AppConfig outputData = getEntityFrom(
                "{\n" +
                        "    \"id\": 1,\n" +
                        "    \"key\": \"period.max.investment.number\",\n" +
                        "    \"value\": \"6\"\n" +
                        "}");
        Mockito.when(mockRepository.findByKey("period.max.investment.number")).thenReturn(Optional.of(outputData));

        // when
        mockMvc.perform(get(entityBaseRequestPath + "?key=period.max.investment.number")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.key", is("period.max.investment.number")))
                .andExpect(jsonPath("$.value", is("6")));

        // then
        verify(mockRepository, times(1)).findByKey(eq("period.max.investment.number"));
    }

    @Test
    public void create_Success() throws Exception {
        // given
        String input =
                "[{\n" +
                "    \"id\": 1,\n" +
                "    \"key\": \"period.max.investment.number\",\n" +
                "    \"value\": \"6\"\n" +
                "},\n" +
                "{\n" +
                "    \"id\": 2,\n" +
                "    \"key\": \"investment.period.length\",\n" +
                "    \"value\": \"2\"\n" +
                "},\n" +
                "{\n" +
                "    \"id\": 3,\n" +
                "    \"key\": \"investment.period.unit\",\n" +
                "    \"value\": \"WEEK\"\n" +
                "}]";
        List<AppConfig> outputData = getEntityListFrom(input);
        Mockito.when(mockRepository.saveAll(any(List.class))).thenReturn(outputData);

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.created_config_ids", is("1, 2, 3")));

        // then
        verify(mockRepository, times(1)).saveAll(any(List.class));
    }

    @Test
    public void update_Success() throws Exception {
        // given
        String input =
                "[{\n" +
                        "    \"id\": 1,\n" +
                        "    \"key\": \"period.max.investment.number\",\n" +
                        "    \"value\": \"7\"\n" +
                        "},\n" +
                        "{\n" +
                        "    \"id\": 2,\n" +
                        "    \"key\": \"investment.period.length\",\n" +
                        "    \"value\": \"3\"\n" +
                        "},\n" +
                        "{\n" +
                        "    \"id\": 3,\n" +
                        "    \"key\": \"investment.period.unit\",\n" +
                        "    \"value\": \"MONTH\"\n" +
                        "}]";
        AppConfig output1 = getEntityFrom(
                "{\n" +
                        "    \"id\": 1,\n" +
                        "    \"key\": \"period.max.investment.number\",\n" +
                        "    \"value\": \"6\"\n" +
                        "}");
        AppConfig output2 = getEntityFrom(
                "{\n" +
                        "    \"id\": 2,\n" +
                        "    \"key\": \"investment.period.length\",\n" +
                        "    \"value\": \"2\"\n" +
                        "}");
        AppConfig output3 =  getEntityFrom(
                "{\n" +
                        "    \"id\": 3,\n" +
                        "    \"key\": \"investment.period.unit\",\n" +
                        "    \"value\": \"WEEK\"\n" +
                        "}");
        List<AppConfig> outputData = getEntityListFrom(input);
        Mockito.when(mockRepository.saveAll(any(List.class))).thenReturn(outputData);
        Mockito.when(mockRepository.findByKey("period.max.investment.number")).thenReturn(Optional.of(output1));
        Mockito.when(mockRepository.findByKey("investment.period.length")).thenReturn(Optional.of(output2));
        Mockito.when(mockRepository.findByKey("investment.period.unit")).thenReturn(Optional.of(output3));

        // when
        mockMvc.perform(put(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.updated_config_ids", is("1, 2, 3")));

        // then
        verify(mockRepository, times(1)).saveAll(any(List.class));
        verify(mockRepository, times(3)).findByKey(any(String.class));
    }

    private List<AppConfig> getEntityListFrom(String jsonInput) throws JsonProcessingException {
        return om.readerForListOf(AppConfig.class).readValue(jsonInput);
    }

    private AppConfig getEntityFrom(String jsonInput) throws JsonProcessingException {
        return om.readerFor(AppConfig.class).readValue(jsonInput);
    }
}