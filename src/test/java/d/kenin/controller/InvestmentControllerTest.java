package d.kenin.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import d.kenin.config.AppConfigSource;
import d.kenin.config.PeriodUnit;
import d.kenin.model.Investment;
import d.kenin.model.InvestmentId;
import d.kenin.model.Investor;
import d.kenin.model.Loan;
import d.kenin.repository.InvestmentRepository;
import d.kenin.repository.InvestorRepository;
import d.kenin.repository.LoanRepository;
import d.kenin.service.FreeGeoIpService;
import d.kenin.service.InvestmentService;
import d.kenin.util.DateUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class InvestmentControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private InvestmentService investmentService;

    @MockBean
    private InvestmentRepository mockRepository;
    @MockBean
    private InvestorRepository mockInvestorRepository;
    @MockBean
    private LoanRepository mockLoanRepository;
    @MockBean
    private FreeGeoIpService freeGeoIpService;
    @MockBean
    private AppConfigSource appConfigSource;

    private static final ObjectMapper om = new ObjectMapper();
    private String entityBaseRequestPath = "/v1/investments";

    @BeforeAll
    public static void beforeAllTests() {
        om.registerModule(new JavaTimeModule());
    }

    @Test
    void getAll_Success() throws Exception {
        // given
        DateUtils.setNow(DateUtils.asInstant(
                DateUtils.asLocalDate("2021-07-02", Loan.PATTERN_LOAN_DATE)));

        Mockito.when(mockRepository.findAll()).thenReturn(prepareInvestorInvestments());

        // when
        mockMvc.perform(get(entityBaseRequestPath)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$"). isArray())
                .andExpect(jsonPath("$[0].investor.id", is(1)))
                .andExpect(jsonPath("$[0].investor.name", is("Dmitrijs")))
                .andExpect(jsonPath("$[0].investor.surname", is("Kenins")))
                .andExpect(jsonPath("$[0].investor.balance", is("250.00")))
                .andExpect(jsonPath("$[0].totalEarned", is(1.51)))
                .andExpect(jsonPath("$[0].loans"). isArray())
                .andExpect(jsonPath("$[0].loans[0].amount", is(150.00)))
                .andExpect(jsonPath("$[0].loans[0].dueDate", is("2021-08-01")))
                .andExpect(jsonPath("$[0].loans[0].amountEarned", is(1.01)))
                .andExpect(jsonPath("$[0].loans[1].amount", is(100.00)))
                .andExpect(jsonPath("$[0].loans[1].dueDate", is("2021-08-01")))
                .andExpect(jsonPath("$[0].loans[1].amountEarned", is(0.50)));

        // then
        verify(mockRepository, times(1)).findAll();
    }

    @Test
    void getByInvestorId_NOT_FOUND() throws Exception {
        // given
        Mockito.when(mockInvestorRepository.findById(25L)).thenReturn(Optional.empty());

        // when
        mockMvc.perform(get(entityBaseRequestPath + "?id=25")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message", is("The investor was not found, requested id: 25")));

        // then
        verify(mockInvestorRepository, times(1)).findById(25L);
        verify(mockRepository, times(0)).findByIdInvestorId(25L);
    }

    @Test
    void getByInvestorId_Success() throws Exception {
        // given
        DateUtils.setNow(DateUtils.asInstant(
                DateUtils.asLocalDate("2021-07-02", Loan.PATTERN_LOAN_DATE)));
        Mockito.when(mockInvestorRepository.findById(1L)).thenReturn(Optional.of(prepareInvestor()));
        Mockito.when(mockRepository.findByIdInvestorId(1L)).thenReturn(
                Collections.unmodifiableList(prepareInvestorInvestments()));

        // when
        mockMvc.perform(get(entityBaseRequestPath + "?id=1")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.investor.id", is(1)))
                .andExpect(jsonPath("$.investor.name", is("Dmitrijs")))
                .andExpect(jsonPath("$.investor.surname", is("Kenins")))
                .andExpect(jsonPath("$.investor.balance", is("250.00")))
                .andExpect(jsonPath("$.totalEarned", is(1.51)))
                .andExpect(jsonPath("$.loans"). isArray())
                .andExpect(jsonPath("$.loans[0].amount", is(150.00)))
                .andExpect(jsonPath("$.loans[0].dueDate", is("2021-08-01")))
                .andExpect(jsonPath("$.loans[0].amountEarned", is(1.01)))
                .andExpect(jsonPath("$.loans[1].amount", is(100.00)))
                .andExpect(jsonPath("$.loans[1].dueDate", is("2021-08-01")))
                .andExpect(jsonPath("$.loans[1].amountEarned", is(0.50)));

        // then
        verify(mockInvestorRepository, times(1)).findById(1L);
        verify(mockRepository, times(1)).findByIdInvestorId(1L);
    }

    @Test
    void create__BAD_REQUEST_on_EMPTY_on_validation() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": null}";

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.timestamp", is(notNullValue())))
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("Validation failed for object='investment'. Error count: 1")))
                .andExpect(jsonPath("$.errors").isMap())
                .andExpect(jsonPath("$.errors.amount", is("is mandatory")));

        // then
        verify(mockRepository, times(0)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(0)).findById(1L);
        verify(mockInvestorRepository, times(0)).findById(1L);
        verify(mockRepository, times(0)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_RANGE_on_validation() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"0\"}";

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Validation failed for object='investment'. Error count: 1")))
                .andExpect(jsonPath("$.errors.amount", is("must be greater than 0")));

        // then
        verify(mockRepository, times(0)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(0)).findById(1L);
        verify(mockInvestorRepository, times(0)).findById(1L);
        verify(mockRepository, times(0)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_FORMAT_on_validation() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"A\"}";

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Validation failed.")))
                .andExpect(jsonPath("$.errors.amount", is("has invalid format: A")));

        // then
        verify(mockRepository, times(0)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(0)).findById(1L);
        verify(mockInvestorRepository, times(0)).findById(1L);
        verify(mockRepository, times(0)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_Exception_AlreadyExists() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        Mockito.when(mockRepository.findById(any(InvestmentId.class))).thenReturn(Optional.of(prepareInvestment()));

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("This investment already exists: loan [1], investor [1]")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(0)).findById(1L);
        verify(mockInvestorRepository, times(0)).findById(1L);
        verify(mockRepository, times(0)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_Exception_ForbiddenInvestment() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        Mockito.when(freeGeoIpService.getCountryCode(any(String.class))).thenReturn("US");

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("Investments are forbidden in your region: US")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(0)).findById(1L);
        verify(mockInvestorRepository, times(0)).findById(1L);
        verify(mockRepository, times(0)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_NOT_FOUND_Loan() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        Mockito.when(freeGeoIpService.getCountryCode(any(String.class))).thenReturn("LV");
        Mockito.when(mockLoanRepository.findById(1L)).thenReturn(Optional.empty());

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message", is("The loan was not found, requested id: 1")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(1)).findById(1L);
        verify(mockInvestorRepository, times(0)).findById(1L);
        verify(mockRepository, times(0)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_NOT_FOUND_Investor() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        Mockito.when(freeGeoIpService.getCountryCode(any(String.class))).thenReturn("LV");
        Mockito.when(mockLoanRepository.findById(1L)).thenReturn(Optional.of(prepareLoan1()));
        Mockito.when(mockInvestorRepository.findById(1L)).thenReturn(Optional.empty());

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.status", is(404)))
                .andExpect(jsonPath("$.message", is("The investor was not found, requested id: 1")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(1)).findById(1L);
        verify(mockInvestorRepository, times(1)).findById(1L);
        verify(mockRepository, times(0)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_Exception_LoanExpired() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        DateUtils.setNow(DateUtils.asInstant(
                DateUtils.asLocalDate("2021-08-02", Loan.PATTERN_LOAN_DATE)));
        Mockito.when(freeGeoIpService.getCountryCode(any(String.class))).thenReturn("LV");
        Mockito.when(mockLoanRepository.findById(1L)).thenReturn(Optional.of(prepareLoan1()));
        Mockito.when(mockInvestorRepository.findById(1L)).thenReturn(Optional.of(prepareInvestor()));

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("The loan term has expired: 2021-08-01")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(1)).findById(1L);
        verify(mockInvestorRepository, times(1)).findById(1L);
        verify(mockRepository, times(0)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_Exception_InvestmentOverflow() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        DateUtils.setNow(DateUtils.asInstant(
                DateUtils.asLocalDate("2021-07-02", Loan.PATTERN_LOAN_DATE)));
        Mockito.when(freeGeoIpService.getCountryCode(any(String.class))).thenReturn("LV");
        Mockito.when(mockLoanRepository.findById(1L)).thenReturn(Optional.of(prepareLoan1()));
        Mockito.when(mockInvestorRepository.findById(1L)).thenReturn(Optional.of(prepareInvestor()));
        Mockito.when(mockRepository.getAvailableAmountToInvest(1L)).thenReturn(Optional.of(new BigDecimal("0.00")));

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("The loan amount is fully covered: 300.00")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(1)).findById(1L);
        verify(mockInvestorRepository, times(1)).findById(1L);
        verify(mockRepository, times(1)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(0)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_Exception_InvestmentLimit() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        DateUtils.setNow(DateUtils.asInstant(
                DateUtils.asLocalDate("2021-07-02", Loan.PATTERN_LOAN_DATE)));
        Mockito.when(freeGeoIpService.getCountryCode(any(String.class))).thenReturn("LV");
        Mockito.when(mockLoanRepository.findById(1L)).thenReturn(Optional.of(prepareLoan1()));
        Mockito.when(mockInvestorRepository.findById(1L)).thenReturn(Optional.of(prepareInvestor()));
        Mockito.when(mockRepository.getAvailableAmountToInvest(1L)).thenReturn(Optional.of(new BigDecimal("150.00")));
        Mockito.when(mockRepository.countInvestments(
                1L, DateUtils.asDate(LocalDate.of(2021, 6, 18))))
                .thenReturn(6);
        Mockito.when(appConfigSource.getInvestmentPeriodDateFrom()).thenCallRealMethod();
        Mockito.when(appConfigSource.getInvestmentPeriodLength()).thenReturn(2);
        Mockito.when(appConfigSource.getInvestmentPeriodUnit()).thenReturn(PeriodUnit.WEEK);
        Mockito.when(appConfigSource.getPeriodMaxInvestmentNumber()).thenReturn(6);

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is(
                        "The investor [1] has reached the limit for the number"
                                + " of investments [6] in the last 2 WEEK")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(1)).findById(1L);
        verify(mockInvestorRepository, times(1)).findById(1L);
        verify(mockRepository, times(1)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(1)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__BAD_REQUEST_on_Exception_InsufficientFunds() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        DateUtils.setNow(DateUtils.asInstant(
                DateUtils.asLocalDate("2021-07-02", Loan.PATTERN_LOAN_DATE)));
        Investor investor = prepareInvestor();
        investor.setBalance(BigDecimal.ZERO);

        Mockito.when(freeGeoIpService.getCountryCode(any(String.class))).thenReturn("LV");
        Mockito.when(mockLoanRepository.findById(1L)).thenReturn(Optional.of(prepareLoan1()));
        Mockito.when(mockInvestorRepository.findById(1L)).thenReturn(Optional.of(investor));
        Mockito.when(mockRepository.getAvailableAmountToInvest(1L)).thenReturn(Optional.of(new BigDecimal("150.00")));
        Mockito.when(mockRepository.countInvestments(
                1L, DateUtils.asDate(LocalDate.of(2021, 6, 18))))
                .thenReturn(1);
        Mockito.when(appConfigSource.getInvestmentPeriodDateFrom()).thenCallRealMethod();
        Mockito.when(appConfigSource.getInvestmentPeriodLength()).thenReturn(2);
        Mockito.when(appConfigSource.getInvestmentPeriodUnit()).thenReturn(PeriodUnit.WEEK);
        Mockito.when(appConfigSource.getPeriodMaxInvestmentNumber()).thenReturn(6);

        // when
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is(
                        "Insufficient funds on the account. Balance is empty.")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(1)).findById(1L);
        verify(mockInvestorRepository, times(1)).findById(1L);
        verify(mockRepository, times(1)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(1)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(0)).save(any(Investment.class));
    }

    @Test
    void create__Success() throws Exception {
        // given
        String input = "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}";
        DateUtils.setNow(DateUtils.asInstant(
                DateUtils.asLocalDate("2021-07-02", Loan.PATTERN_LOAN_DATE)));
        Investor investor = prepareInvestor();
        investor.setBalance(new BigDecimal("50.00"));
        Loan loan = prepareLoan1();
        Investment investment = prepareInvestment();
        investment.setAmount(new BigDecimal("50.00"));

        Mockito.when(mockLoanRepository.save(any(Loan.class))).thenReturn(loan);
        Mockito.when(mockInvestorRepository.save(any(Investor.class))).thenReturn(investor);
        Mockito.when(mockRepository.save(any(Investment.class))).thenReturn(investment);
        Mockito.when(appConfigSource.getInvestmentPeriodDateFrom()).thenCallRealMethod();

        Mockito.when(freeGeoIpService.getCountryCode(any(String.class))).thenReturn("LV");
        Mockito.when(mockLoanRepository.findById(1L)).thenReturn(Optional.of(loan));
        Mockito.when(mockInvestorRepository.findById(1L)).thenReturn(Optional.of(investor));
        Mockito.when(mockRepository.getAvailableAmountToInvest(1L)).thenReturn(Optional.of(new BigDecimal("150.00")));
        Mockito.when(mockRepository.countInvestments(
                1L, DateUtils.asDate(LocalDate.of(2021, 6, 18))))
                .thenReturn(1);
        Mockito.when(appConfigSource.getInvestmentPeriodLength()).thenReturn(2);
        Mockito.when(appConfigSource.getInvestmentPeriodUnit()).thenReturn(PeriodUnit.WEEK);
        Mockito.when(appConfigSource.getPeriodMaxInvestmentNumber()).thenReturn(6);

        // when
        mockInvestorRepository.save(investor);
        mockLoanRepository.save(loan);
        mockMvc.perform(post(entityBaseRequestPath)
                .content(input)
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Basic YWRtaW46MTIz")) //admin123
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['investor_id\\, amount']", is("1, 50.00")))
                .andExpect(jsonPath("$.loan_id", is("1")));

        // then
        verify(mockRepository, times(1)).findById(any(InvestmentId.class));
        verify(mockLoanRepository, times(1)).findById(1L);
        verify(mockInvestorRepository, times(1)).findById(1L);
        verify(mockRepository, times(1)).getAvailableAmountToInvest(1L);
        verify(mockRepository, times(1)).countInvestments(any(Long.class), any(Date.class));
        verify(mockRepository, times(1)).save(any(Investment.class));
    }

    private Investment prepareInvestment() throws JsonProcessingException {
        Loan loan1 = prepareLoan1();
        Investor investor = prepareInvestor();
        Investment investment = getEntityFrom(
                "{\"investor_id\": 1, \"loan_id\": 1, \"amount\": \"100.00\"}",
                Investment.class);
        investment.setCreateDate(
                DateUtils.asDate(LocalDate.of(2021, 7, 1)));
        investment.getId().setInvestor(investor);
        investment.getId().setLoan(loan1);
        return investment;
    }

    private List<Investment> prepareInvestorInvestments() throws JsonProcessingException {
        Loan loan1 = prepareLoan1();
        Loan loan2 = prepareLoan2();
        Investor investor = prepareInvestor();
        List<Investment> outputData = getEntityListFrom(
                "[{\"investor_id\": 1,\"loan_id\": 1,\"amount\": \"150.00\"}," +
                        "{\"investor_id\": 1, \"loan_id\": 2, \"amount\": \"100.00\"}]");

        outputData.get(0).setCreateDate(
                DateUtils.asDate(LocalDate.of(2021, 7, 1)));
        outputData.get(0).getId().setInvestor(investor);
        outputData.get(0).getId().setLoan(loan1);
        outputData.get(1).setCreateDate(
                DateUtils.asDate(LocalDate.of(2021, 7, 1)));
        outputData.get(1).getId().setInvestor(investor);
        outputData.get(1).getId().setLoan(loan2);
        return outputData;
    }

    private Loan prepareLoan1() throws JsonProcessingException {
        return getEntityFrom(
                "{\"id\": 1, \"amount\": 300.00, \"dueDate\": \"2021-08-01\", \"monthlyRate\": 0.2}",
                Loan.class);
    }

    private Loan prepareLoan2() throws JsonProcessingException {
        return getEntityFrom(
                "{\"id\": 2, \"amount\": 500.00, \"dueDate\": \"2021-08-01\", \"monthlyRate\": 0.15}",
                Loan.class);
    }

    private Investor prepareInvestor() throws JsonProcessingException {
        return getEntityFrom(
                "{\"id\": 1, \"name\": \"Dmitrijs\", \"surname\": \"Kenins\", \"balance\": 250.00}",
                Investor.class);
    }

    private List<Investment> getEntityListFrom(String jsonInput) throws JsonProcessingException {
        return om.readerForListOf(Investment.class).readValue(jsonInput);
    }

    private <T> T getEntityFrom(String jsonInput, Class<T> clazz) throws JsonProcessingException {
        return om.readerFor(clazz).readValue(jsonInput);
    }
}