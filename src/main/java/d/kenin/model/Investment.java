package d.kenin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "investment")
public class Investment {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";

    @JsonUnwrapped
    @EmbeddedId
    private InvestmentId id;

    @NotNull(message = ERROR_MSG_IS_MANDATORY)
    @Positive
    @Digits(integer = 15, fraction = 2)
    @Column(nullable = false, precision = 17, scale = 2)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal amount;

    @JsonIgnore
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable=false)
    private Date createDate;

    public Long getInvestorId() {
        return getId().getInvestor().getId();
    }

    public Long getLoanId() {
        return getId().getLoan().getId();
    }

}
