package d.kenin.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name = "app_config")
public class AppConfig {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = ERROR_MSG_IS_MANDATORY)
    @Column(nullable = false, length = 150, unique = true)
    private String key;

    private String value;

}
