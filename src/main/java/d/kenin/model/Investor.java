package d.kenin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "investor")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Investor {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = ERROR_MSG_IS_MANDATORY)
    @Column(nullable = false, length = 150)
    private String name;

    @NotBlank(message = ERROR_MSG_IS_MANDATORY)
    @Column(nullable = false, length = 150)
    private String surname;

    @NotNull(message = ERROR_MSG_IS_MANDATORY)
    @Min(0)
    @Digits(integer = 15, fraction = 2)
    @Column(nullable = false, precision = 17, scale = 2)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal balance;

}
