package d.kenin.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonSetter;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Data
@Embeddable
public class InvestmentId implements Serializable {

    @JsonAlias("investor_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "investor_id", nullable = false, referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Investor investor;

    @JsonAlias("loan_id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_id", nullable = false, referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Loan loan;

    @JsonSetter
    public void setInvestor(long id) {
        this.investor = new Investor();
        this.investor.setId(id);
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    @JsonSetter
    public void setLoan(long id) {
        this.loan = new Loan();
        this.loan.setId(id);
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }
}
