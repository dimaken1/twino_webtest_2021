package d.kenin.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "loan")
public class Loan {
    private static final String ERROR_MSG_IS_MANDATORY = "is mandatory";
    public static final String PATTERN_LOAN_DATE = "yyyy-MM-dd";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = ERROR_MSG_IS_MANDATORY)
    @Positive
    @Digits(integer = 15, fraction = 2)
    @Column(nullable = false, precision = 17, scale = 2)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal amount;

    @NotNull(message = ERROR_MSG_IS_MANDATORY)
    @Column(name = "due_date", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = PATTERN_LOAN_DATE)
    private LocalDate dueDate;

    @NotNull(message = ERROR_MSG_IS_MANDATORY)
    @Positive
    @Digits(integer = 3, fraction = 4)
    @Column(nullable = false, precision = 7, scale = 4)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal monthlyRate;

}
