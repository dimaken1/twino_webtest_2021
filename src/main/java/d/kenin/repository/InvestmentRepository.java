package d.kenin.repository;

import d.kenin.model.Investment;
import d.kenin.model.InvestmentId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface InvestmentRepository extends JpaRepository<Investment, InvestmentId> {

   List<Investment> findByIdInvestorId(long investorId);

    @Query(value =
            "SELECT count(*)"
                    + " FROM investment"
                    + " WHERE investor_id = :investorId"
                    + "   AND create_date >= :dateFrom",
            nativeQuery = true)
    int countInvestments(@Param("investorId") long investorId,
                         @Param("dateFrom") Date from);

    @Query(value =
            "SELECT MAX(l.amount) - SUM(i.amount) as amount"
                    + " FROM loan l"
                    + " INNER JOIN investment i ON i.loan_id = l.id"
                    + " WHERE l.id = :loanId",
            nativeQuery = true)
    Optional<BigDecimal> getAvailableAmountToInvest(@Param("loanId") long loanId);

//    @Transactional
//    @Modifying
//    @Query (value =
//            "UPDATE investor i"
//                    + " SET balance = balance - :amount"
//                    + " WHERE i.id = :investorId",
//          nativeQuery = true)
//    void withdrawInvestorFunds(@Param("investorId") long investorId,
//                               @Param("amount") BigDecimal amount);
}
