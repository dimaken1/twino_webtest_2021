package d.kenin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.math.BigDecimal;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvestmentOverflowException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvestmentOverflowException(BigDecimal loanAmount) {
        super("The loan amount is fully covered: " + loanAmount.toPlainString());
    }

}
