package d.kenin.exception;

import d.kenin.model.Loan;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class LoanExpiredException extends Exception {

    private static final long serialVersionUID = 1L;

    private static DateTimeFormatter df = DateTimeFormatter.ofPattern(Loan.PATTERN_LOAN_DATE);

    public LoanExpiredException(LocalDate expirationDate) {
        super("The loan term has expired: " + expirationDate.format(df));
    }

}
