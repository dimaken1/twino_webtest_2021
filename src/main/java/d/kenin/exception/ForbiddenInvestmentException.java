package d.kenin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ForbiddenInvestmentException extends Exception {

    private static final long serialVersionUID = 1L;

    public ForbiddenInvestmentException(String countryCode) {
        super("Investments are forbidden in your region: " + countryCode);
    }

}
