package d.kenin.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.*;
import java.util.stream.Collectors;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    public static final Logger log = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        Map<String, String> errors = new LinkedHashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error -> {
            if (errors.containsKey(error.getField())){
                errors.put(
                        error.getField(),
                        errors.get(error.getField()) + "; " + error.getDefaultMessage());
            } else {
                errors.put(error.getField(), error.getDefaultMessage());
            }
        });

        String message = "Validation failed for object='"
                + ex.getObjectName() + "'. Error count: " +  errors.size();

        return buildExceptionResponse(
                headers, HttpStatus.BAD_REQUEST, request, message, errors);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        ResponseEntity<Object> exceptionResponse = null;
        if (ex.contains(InvalidFormatException.class)) {
            Throwable fex = getCauseIfExists(ex, InvalidFormatException.class);
            if (fex != null) {
                try {
                    exceptionResponse = handleExceptionsForCustomization((Exception) fex, request);
                } catch (Exception e) {
                    log.error(e.toString(), e);
                }
            }
        }
        if (exceptionResponse == null) {
            exceptionResponse = super.handleHttpMessageNotReadable(ex, headers, status, request);
        }
        return exceptionResponse;
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex,
                                                             @Nullable Object body,
                                                             HttpHeaders headers,
                                                             HttpStatus status,
                                                             WebRequest request) {
        log.error(ex.toString(), ex);
        return buildExceptionResponse(headers, status, request, ex.toString(), null);
    }

    @ExceptionHandler({ResourceNotFoundException.class, LoanExpiredException.class,
            InvestmentOverflowException.class, InvestmentLimitException.class,
            InvalidFormatException.class, ConstraintViolationException.class,
            InsufficientFundsException.class, ForbiddenInvestmentException.class,
            ResourceAlreadyExistsException.class,
            Throwable.class})
    public ResponseEntity<Object> handleExceptionsForCustomization(Exception ex,
                                                                   WebRequest request) {

        log.error(ex.toString(), ex);
        HttpStatus newStatus = HttpStatus.BAD_REQUEST;
        String message = ex.getMessage();
        Map<String, String> errors = new LinkedHashMap<>();

        if (ex instanceof ResourceNotFoundException) {
            newStatus = HttpStatus.NOT_FOUND;

        } else if (ex instanceof LoanExpiredException
                || ex instanceof InvestmentOverflowException
                || ex instanceof InvestmentLimitException
                || ex instanceof InsufficientFundsException
                || ex instanceof ForbiddenInvestmentException
                || ex instanceof ResourceAlreadyExistsException) {
            // nothing to do
        } else if (ex instanceof InvalidFormatException) {
            InvalidFormatException fex = (InvalidFormatException) ex;
            String fieldPath = fex.getPath().stream()
                    .map(JsonMappingException.Reference::getFieldName)
                    .collect(Collectors.joining("."));
            errors.put(fieldPath, "has invalid format: " + fex.getValue());
            message = "Validation failed.";

        } else if (ex instanceof ConstraintViolationException) {
            ConstraintViolationException cvex = (ConstraintViolationException) ex;
            Set<String> failedEntities = new HashSet<>();
            cvex.getConstraintViolations().forEach(cv -> {
                failedEntities.add(cv.getRootBeanClass().getSimpleName());
                if (errors.containsKey(cv.getPropertyPath().toString())){
                    errors.put(
                            cv.getPropertyPath().toString(),
                            errors.get(cv.getPropertyPath().toString()) + "; " + cv.getMessage());
                } else {
                    errors.put(cv.getPropertyPath().toString(), cv.getMessage());
                }
            });
            message = "Validation failed for entities: "
                    + String.join(",", failedEntities)
                    +  ". Error count: " + errors.size();

        } else {
            newStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            message = ex.toString();
        }
        return buildExceptionResponse(
                new HttpHeaders(), newStatus, request, message, errors);
    }

    protected ResponseEntity<Object> buildExceptionResponse(HttpHeaders headers,
                                                            HttpStatus status,
                                                            WebRequest request,
                                                            String message,
                                                            Map<String, String> errors) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());
        body.put("error", status.getReasonPhrase());
        body.put("message", message);
        if (errors != null && !errors.isEmpty()) {
            body.put("errors", errors);
        }
        body.put("path", ((ServletWebRequest)request).getRequest().getRequestURI());
        return new ResponseEntity<>(body, headers, status);
    }

    protected Throwable getCauseIfExists(Throwable exception, Class<? extends Throwable>... searchList) {
        Throwable cause = exception.getCause();
        Throwable result = null;
        while(cause != null) {
            for (Class<?> searchItem : searchList) {
                if (searchItem.isInstance(cause)) {
                    result = cause;
                    break;
                }
            }
            if (result != null || cause.getCause() == cause) {
                break;
            }
            cause = cause.getCause();
        }
        return result;
    }

}
