package d.kenin.exception;

import d.kenin.config.PeriodUnit;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvestmentLimitException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvestmentLimitException(long investorId,
                                    int maxInvestmentNumber,
                                    int periodLength,
                                    PeriodUnit periodUnit) {
        super("The investor [" + investorId
                + "] has reached the limit for the number of investments [" + maxInvestmentNumber
                + "] in the last " + periodLength + " " + periodUnit);
    }

}
