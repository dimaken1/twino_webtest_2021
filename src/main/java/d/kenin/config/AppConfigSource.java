package d.kenin.config;

import d.kenin.model.AppConfig;
import d.kenin.service.AppConfigService;
import d.kenin.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class AppConfigSource {
    public static final String PROP_PERIOD_MAX_INVESTMENT_NUMBER = "period.max.investment.number";
    public static final String PROP_INVESTMENT_PERIOD_LENGTH = "investment.period.length";
    public static final String PROP_INVESTMENT_PERIOD_UNIT = "investment.period.unit";

    public static final int DEFAULT_PERIOD_MAX_INVESTMENT_NUMBER = 5;
    public static final int DEFAULT_INVESTMENT_PERIOD_LENGTH = 1;
    public static final String DEFAULT_INVESTMENT_PERIOD_UNIT = "WEEK"; // DAY, WEEK, MONTH, YEAR

    public static final Logger log = LoggerFactory.getLogger(AppConfigSource.class);

    @Autowired
    private AppConfigService service;

    private Map<String, String> properties = new ConcurrentHashMap<>();

    public void initialize() {
        reload();
        Thread thread = new Thread(this::loop);
        thread.start();
    }

    public String getValue(String key) {
        return getValue(key, null);
    }

    public int getValue(String key, int defaultValue) {
        return Integer.parseInt(getValue(key, defaultValue + ""));
    }

    public String getValue(String key, String defaultValue) {
        return properties.getOrDefault(key, defaultValue);
    }

    public Date getInvestmentPeriodDateFrom() {
        LocalDateTime dateFrom = DateUtils.getNowAsLocalDateTime();
        int periodLength = getInvestmentPeriodLength();
        PeriodUnit unit = getInvestmentPeriodUnit();
        switch (unit) {
            case DAY:
                dateFrom = dateFrom.minusDays(periodLength);
                break;
            case WEEK:
                dateFrom = dateFrom.minusWeeks(periodLength);
                break;
            case MONTH:
                dateFrom = dateFrom.minusMonths(periodLength);
                break;
            case YEAR:
                dateFrom = dateFrom.minusYears(periodLength);
        }
        return DateUtils.asDate(dateFrom);
    }

    public int getPeriodMaxInvestmentNumber() {
        return getValue(PROP_PERIOD_MAX_INVESTMENT_NUMBER, DEFAULT_PERIOD_MAX_INVESTMENT_NUMBER);
    }

    public int getInvestmentPeriodLength() {
        return getValue(PROP_INVESTMENT_PERIOD_LENGTH, DEFAULT_INVESTMENT_PERIOD_LENGTH);
    }

    public PeriodUnit getInvestmentPeriodUnit() {
        return PeriodUnit.valueOf(
                getValue(PROP_INVESTMENT_PERIOD_UNIT, DEFAULT_INVESTMENT_PERIOD_UNIT));
    }

    // Private part
    private void reload() {
        try {
            List<AppConfig> configs = service.getAll();
            properties.putAll(configs
                    .stream()
                    .collect(Collectors.toMap(
                            AppConfig::getKey, AppConfig::getValue,
                            (o, n) -> o,
                            HashMap::new)));
        } catch (Exception ex) {
            log.error(ex.toString(), ex);
        }
    }

    private void loop() {
        while(true) {
            try {
                Thread.sleep(60000);
                reload();
            } catch (Exception ex) {}
        }
    }
}
