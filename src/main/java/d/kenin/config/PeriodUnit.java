package d.kenin.config;

public enum PeriodUnit {
    DAY, WEEK, MONTH, YEAR
}
