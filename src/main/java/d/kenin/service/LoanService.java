package d.kenin.service;

import d.kenin.model.Loan;
import d.kenin.repository.LoanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoanService {

    @Autowired
    private LoanRepository repository;

    public List<Loan> getAll() {
        return repository.findAll();
    }

    public long create(Loan loan) {
        loan = repository.save(loan);
        return loan.getId();
    }

}
