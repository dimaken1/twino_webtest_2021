package d.kenin.service;

import d.kenin.config.AppConfigSource;
import d.kenin.exception.*;
import d.kenin.model.Investment;
import d.kenin.model.Investor;
import d.kenin.domain.InvestorLoans;
import d.kenin.model.Loan;
import d.kenin.repository.InvestmentRepository;
import d.kenin.repository.InvestorRepository;
import d.kenin.repository.LoanRepository;
import d.kenin.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static d.kenin.service.FreeGeoIpService.DEFAULT_COUNTRY_CODE;

@Service
public class InvestmentService {
    private static final String ERROR_MSG_RESOURCE_NOT_FOUND = "The {0} was not found, requested id: {1}";

    @Autowired
    private InvestmentRepository repository;

    @Autowired
    private LoanRepository loanRepository;

    @Autowired
    private InvestorRepository investorRepository;

    @Autowired
    private AppConfigSource appConfigSource;

    @Autowired
    private FreeGeoIpService freeGeoIpService;

    public List<InvestorLoans> getAll() {
        List<Investment> investments = repository.findAll();
        Map<Investor, List<Investment>> result = investments.stream()
                .collect(Collectors.groupingBy(investment -> investment.getId().getInvestor()));
        return result.entrySet().stream()
                .map(entry -> InvestorLoans.build(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    public InvestorLoans getByInvestorId(long id) {
        Investor investor = investorRepository.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(
                        MessageFormat.format(ERROR_MSG_RESOURCE_NOT_FOUND,"investor", id)));
        List<Investment> investments = repository.findByIdInvestorId(id);
        return InvestorLoans.build(investor, investments);
    }

    @Transactional
    public Investment create(Investment investment, String remoteIp) throws
            LoanExpiredException, InvestmentOverflowException,
            InvestmentLimitException, InsufficientFundsException,
            ForbiddenInvestmentException, ResourceAlreadyExistsException {
        if (repository.findById(investment.getId()).isPresent()) {
            throw new ResourceAlreadyExistsException("This investment already exists: loan ["
                    + investment.getLoanId() + "], investor [" + investment.getInvestorId() + "]");
        }

        String remoteCountry = freeGeoIpService.getCountryCode(remoteIp);
        if (!DEFAULT_COUNTRY_CODE.equalsIgnoreCase(remoteCountry)) {
            throw new ForbiddenInvestmentException(remoteCountry);
        }

        long loanId = investment.getLoanId();
        Loan loan = loanRepository.findById(loanId).orElseThrow(() ->
                new ResourceNotFoundException(
                        MessageFormat.format(ERROR_MSG_RESOURCE_NOT_FOUND, "loan", loanId)));

        long investorId = investment.getInvestorId();
        Investor investor = investorRepository.findById(investorId).orElseThrow(() ->
                new ResourceNotFoundException(
                        MessageFormat.format(ERROR_MSG_RESOURCE_NOT_FOUND,"investor", investorId)));

        investment.getId().setLoan(loan);
        investment.getId().setInvestor(investor);
        synchronized ("Loan_" + loan.getId()) {
            synchronized ("Investor_" + investor.getId()) {
                // validations
                BigDecimal availableAmountToInvest = checkLoanState(loan);
                if (availableAmountToInvest.compareTo(investment.getAmount()) < 0) {
                    investment.setAmount(availableAmountToInvest);
                }
                checkInvestorLimits(investment);

                return registerInvestment(investment);
            }
        }
    }

    private BigDecimal checkLoanState(Loan loan) throws LoanExpiredException, InvestmentOverflowException {
        if (loan.getDueDate().isBefore(DateUtils.getNowAsLocalDate())) {
            throw new LoanExpiredException(loan.getDueDate());
        }
        BigDecimal availableAmountToInvest = repository
                .getAvailableAmountToInvest(loan.getId())
                .orElse(loan.getAmount());
        if (availableAmountToInvest.signum() <= 0) {
            throw new InvestmentOverflowException(loan.getAmount());
        }
        return availableAmountToInvest;
    }

    private void checkInvestorLimits(Investment investment) throws InvestmentLimitException, InsufficientFundsException {
        Investor investor = investment.getId().getInvestor();
        Date investmentDateFrom = appConfigSource.getInvestmentPeriodDateFrom();
        int maxInvestmentNumber = appConfigSource.getPeriodMaxInvestmentNumber();
        if (repository.countInvestments(investor.getId(), investmentDateFrom)
                >= maxInvestmentNumber) {
            throw new InvestmentLimitException(
                    investor.getId(),
                    maxInvestmentNumber,
                    appConfigSource.getInvestmentPeriodLength(),
                    appConfigSource.getInvestmentPeriodUnit());
        }
        if (investor.getBalance().signum() <= 0) {
            throw new InsufficientFundsException();
        }
        if (investor.getBalance().compareTo(investment.getAmount()) < 0) {
            investment.setAmount(investor.getBalance());
        }
    }

    private Investment registerInvestment(Investment investment) {
        Investor investor = investment.getId().getInvestor();
        investor.setBalance(investor.getBalance().subtract(investment.getAmount()));
        //repository.withdrawInvestorFunds(investment.getInvestorId(), investment.getAmount());
        return repository.save(investment);
    }
}
