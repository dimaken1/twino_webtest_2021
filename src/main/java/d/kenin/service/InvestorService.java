package d.kenin.service;

import d.kenin.model.Investor;
import d.kenin.repository.InvestorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvestorService {

    @Autowired
    private InvestorRepository repository;

    public List<Investor> getAll() {
        return repository.findAll();
    }

    public long create(Investor investor) {
        investor = repository.save(investor);
        return investor.getId();
    }
}
