package d.kenin.service;

import d.kenin.exception.ResourceNotFoundException;
import d.kenin.model.AppConfig;
import d.kenin.repository.AppConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AppConfigService {
    private static final String ERROR_MSG_RESOURCE_NOT_FOUND = "The {0} was not found, requested key: {1}";

    @Autowired
    private AppConfigRepository repository;

    public List<AppConfig> getAll() {
        return repository.findAll();
    }

    public AppConfig getByKey(String key) {
        Optional<AppConfig> opConfig = repository.findByKey(key);
        return opConfig.orElseThrow(() ->
                new ResourceNotFoundException(
                        MessageFormat.format(ERROR_MSG_RESOURCE_NOT_FOUND,"property", key)));
    }

    public String create(List<AppConfig> configs) {
        configs = repository.saveAll(configs);
        return getConfigIds(configs);
    }

    public String update(List<AppConfig> configs) {
        configs = findAllByKey(configs);
        configs = repository.saveAll(configs);
        return getConfigIds(configs);
    }

    private List<AppConfig> findAllByKey(List<AppConfig> configs) {
        return configs.stream()
                .map(config -> {
                    AppConfig newConfig = repository.findByKey(config.getKey()).orElse(null);
                    if (newConfig != null) {
                        newConfig.setValue(config.getValue());
                    }
                    return newConfig;
                })
                .filter(config -> config != null)
                .collect(Collectors.toList());
    }

    private String getConfigIds(List<AppConfig> configs) {
        return configs.stream()
                .map(config -> Long.toString(config.getId()))
                .collect(Collectors.joining(", "));
    }

}
