package d.kenin.service;

import d.kenin.domain.FreeGeoIpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class FreeGeoIpService {
    public static final String DEFAULT_COUNTRY_CODE = "LV";
    public static final String url = "https://freegeoip.app/json/{ip}";

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;

    public String getCountryCode(String ipAddress) {
        String result = DEFAULT_COUNTRY_CODE;
        ResponseEntity<FreeGeoIpResponse> response = getRestTemplate().getForEntity(
                url, FreeGeoIpResponse.class, ipAddress);
        if (response.getStatusCode() == HttpStatus.OK) {
            try {
                result = response.getBody().getCountryCode();
            } catch (Exception ex) {}
            if (result == null || result.trim().isEmpty()) {
                result = DEFAULT_COUNTRY_CODE;
            }
        }
        return result;
    }

    private RestTemplate getRestTemplate() {
        return restTemplateBuilder.build();
    }

}
