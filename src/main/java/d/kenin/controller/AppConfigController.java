package d.kenin.controller;

import d.kenin.model.AppConfig;
import d.kenin.service.AppConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@Validated
@RequestMapping("/v1/config")
public class AppConfigController {

    @Autowired
    private AppConfigService service;

    @GetMapping
    public List<AppConfig> getAll() {
        return service.getAll();
    }

    @GetMapping(params = {"key"})
    public AppConfig getByKey(@RequestParam("key") String key) {
        return service.getByKey(key);
    }

    @PostMapping
    public Map<String, String> create(@RequestBody List<@Valid AppConfig> configs) {
        String ids = service.create(configs);
        return Collections.singletonMap("created_config_ids", ids);
    }

    @PutMapping
    public Map<String, String> update(@RequestBody List<@Valid AppConfig> configs) {
        String ids = service.update(configs);
        return Collections.singletonMap("updated_config_ids", ids);
    }

}
