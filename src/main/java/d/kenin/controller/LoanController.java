package d.kenin.controller;

import d.kenin.model.Loan;
import d.kenin.service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/loans")
public class LoanController {

    @Autowired
    private LoanService service;

    @GetMapping("")
    public List<Loan> getAll() {
        return service.getAll();
    }

    @PostMapping("")
    public Map<String, Long> create(@Valid @RequestBody Loan loan) {
        long id = service.create(loan);
        return Collections.singletonMap("id", id);
    }

}
