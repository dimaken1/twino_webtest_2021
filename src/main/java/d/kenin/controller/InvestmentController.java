package d.kenin.controller;

import d.kenin.exception.*;
import d.kenin.model.Investment;
import d.kenin.domain.InvestorLoans;
import d.kenin.service.InvestmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/investments")
public class InvestmentController {

    @Autowired
    private InvestmentService service;

    @GetMapping("")
    public List<InvestorLoans> getAll() {
        return service.getAll();
    }

    @GetMapping(params = {"id"})
    public InvestorLoans getByInvestorId(@RequestParam("id") long id) {
        return service.getByInvestorId(id);
    }

    @PostMapping("")
    public Map<String, String> create(@Valid @RequestBody Investment investment,
                                      HttpServletRequest request) throws
            LoanExpiredException, InvestmentOverflowException, InvestmentLimitException,
            InsufficientFundsException, ForbiddenInvestmentException, ResourceAlreadyExistsException {
        investment = service.create(investment, request.getRemoteAddr());
        Map<String, String> result = new HashMap<>();
        result.put(
                "investor_id, amount",
                investment.getInvestorId() + ", " + investment.getAmount().toPlainString());
        result.put(
                "loan_id",
                investment.getLoanId() + "");
        return result;
    }
}
