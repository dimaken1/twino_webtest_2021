package d.kenin.controller;

import d.kenin.model.Investor;
import d.kenin.service.InvestorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/investors")
public class InvestorController {

    @Autowired
    private InvestorService service;

    @GetMapping("")
    public List<Investor> getAll() {
        return service.getAll();
    }

    @PostMapping("")
    public Map<String, Long> create(@Valid @RequestBody Investor investor) {
        long id = service.create(investor);
        return Collections.singletonMap("id", id);
    }

}
