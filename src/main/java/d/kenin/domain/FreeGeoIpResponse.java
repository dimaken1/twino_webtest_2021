package d.kenin.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class FreeGeoIpResponse {

    private String ip;

    @JsonAlias("country_code")
    private String countryCode;

    @JsonAlias("country_name")
    private String countryName;
}
