package d.kenin.domain;

import d.kenin.model.Investment;
import d.kenin.model.Investor;
import d.kenin.util.DateUtils;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@ToString
public class InvestorLoans {

    private Investor investor;
    private BigDecimal totalEarned;
    private List<InvestorLoan> loans;

    public static InvestorLoans build(Investor investor, List<Investment> investments) {
        InvestorLoans result = new InvestorLoans();
        result.investor = investor;
        result.totalEarned = BigDecimal.ZERO;
        result.loans = investments.stream()
                .map(investment -> {
                    InvestorLoan loan = InvestorLoan.build(investment);
                    result.totalEarned = result.totalEarned.add(loan.getAmountEarned());
                    return loan;
                })
                .collect(Collectors.toList());
        return result;
    }

    @Getter
    @ToString
    public static class InvestorLoan {

        private BigDecimal amount;
        private LocalDate dueDate;
        private BigDecimal amountEarned;

        public static InvestorLoan build(Investment investment) {
            final int DAYS_IN_MONTH = 30;
            InvestorLoan result = new InvestorLoan();
            result.amount = investment.getAmount();
            result.dueDate = investment.getId().getLoan().getDueDate();

            LocalDate beginDate = DateUtils.asLocalDate(investment.getCreateDate());
            LocalDate endDate = DateUtils.getNowAsLocalDate();
            if (result.getDueDate().isBefore(endDate)) {
                endDate = result.getDueDate();
            }
            long loanDays = ChronoUnit.DAYS.between(beginDate, endDate);
            result.amountEarned = investment.getId().getLoan().getMonthlyRate()
                    .divide(new BigDecimal(DAYS_IN_MONTH), MathContext.DECIMAL64)
                    .multiply(investment.getAmount())
                    .multiply(new BigDecimal(loanDays + ""))
            .setScale(2, RoundingMode.UP);
            return result;
        }
    }

}
