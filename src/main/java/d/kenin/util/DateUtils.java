package d.kenin.util;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateUtils {
    private static Instant testNow = null;

    public static void setNow(Instant now) {
        testNow = now;
    }

    public static Instant getNow() {
        if (testNow != null) {
            return testNow;
        } else {
            return Instant.now();
        }
    }

    public static LocalDate getNowAsLocalDate() {
        return asLocalDate(getNow());
    }

    public static LocalDateTime getNowAsLocalDateTime() {
        return asLocalDateTime(getNow());
    }

    public static Instant asInstant(LocalDate localDate) {
        return localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(asInstant(localDate));
    }

    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDate asLocalDate(Date date) {
        return asLocalDate(date.toInstant());
    }

    public static LocalDate asLocalDate(Instant instant) {
        return instant.atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static LocalDate asLocalDate(String strDate, String dateTimeFormat) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(dateTimeFormat);
        return LocalDate.parse(strDate, df);
    }

    public static LocalDateTime asLocalDateTime(Instant instant) {
        return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
